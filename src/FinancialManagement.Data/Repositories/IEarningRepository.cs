﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FinancialManagement.Domain.Entities;

namespace FinancialManagement.Data.Repositories
{
    public interface IEarningRepository
    {
        void Add(Earning earning);
        Task<IEnumerable<Earning>> GetAll();
    }
}
