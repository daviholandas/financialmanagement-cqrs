﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinancialManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace FinancialManagement.Data.Repositories
{
    public class EarningRepository : IEarningRepository
    {
        private readonly ApplicationDbContext _context;

        public EarningRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(Earning earning)
        {
            _context.Add(earning);
            _context.SaveChangesAsync().Wait();
        }

        public async Task<IEnumerable<Earning>> GetAll()
            => await _context.Earnings.ToListAsync();
    }
}
