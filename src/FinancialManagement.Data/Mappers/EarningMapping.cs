﻿using System;
using System.Collections.Generic;
using System.Text;
using FinancialManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinancialManagement.Data.Mappers
{
    class EarningMapping : IEntityTypeConfiguration<Earning>
    {
        public void Configure(EntityTypeBuilder<Earning> builder)
        {
            builder.ToTable("Earnings");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Description)
                .HasColumnName("Descricao")
                .HasColumnType("varchar(200)");
            builder.Property(e => e.Value)
                .HasColumnName("Valor")
                .HasColumnType("decimal(15,2)");
            builder.Property(e => e.ReceivedDate)
                .HasColumnName("DataRecebimento");
            builder.Property(e => e.Received)
                .HasColumnName("Recebido");
        }
    }
}
