﻿using System;
using System.Collections.Generic;
using System.Text;
using FinancialManagement.Domain.DomainObjects;

namespace FinancialManagement.Domain.Entities
{
    public class Earning : Entity
    {
        public Earning(string description, decimal value, DateTime receivedDate, bool received)
        {
            Description = description;
            Value = value;
            ReceivedDate = receivedDate;
            Received = received;
        }

        private Earning(){}

        public string Description { get; private set; }
        public decimal Value { get; private set; }
        public DateTime ReceivedDate { get; private set; }
        public bool Received { get; private set; }
    }
}
