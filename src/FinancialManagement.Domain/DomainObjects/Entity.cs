﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialManagement.Domain.DomainObjects
{
    public abstract  class Entity
    {
        protected Entity()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; protected set; }
    }
}
